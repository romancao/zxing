## Version 2.0.3
- 正式版本

## Version 2.0.3-rc.1
1.增加扫一扫和相册图片扫描

## Version 2.0.3-rc.0
1.修复round接口内的无效判断

## Version 2.0.2
1.适配ArkTs语法

2.适配DevEco Studio: 4.0 Beta2(4.0.3.500), SDK: API10 (4.0.10.7)

## Version 2.0.1

1.适配DevEco Studio: 4.0 Canary2(4.0.3.317), SDK: API10 (4.0.9.5)
2.修正CodaBar码可以解析内容只有一个字符能力
3.修正HighLevelEncoder类里静态对象的使用方式


## Version 2.0.0

1.适配DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)
2.包管理工具由npm切换成ohpm

## 1.0.8
1.添加关键字。

## 1.0.7
1.适配DevEco Studio 3.1 Beta1版本。

## 1.0.6
1.适配STAGE,FA模型。

## 1.0.5
1.删除module里多余的MainPage.ets页面。

## 1.0.3
1.适配api9。

## 1.0.2
1.由gradle工程整改为hvigor工程。

## 1.0.1
1.适配ark引擎。

## 1.0.0
1.支持 Code 39、 Code 93、Code 128、Codabar 码生成功能。

2.支持 ITF、 UPC-E、UPC-A、EAN-8、EAN-13 码生成功能。

3.支持MaxiCode、Code 39、Codabar 码解析功能。

4.支持 RSS-14、RSS-Expanded、QR Code、Data Matrix、Aztec、PDF 417码格式功能。