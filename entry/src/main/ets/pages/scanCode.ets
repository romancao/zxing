/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CameraView, CameraService } from '@ohos/zxing'
import router from '@ohos.router'

@Entry
@Component
struct ScanCode {
  @StorageProp('qrCodeParseResult') @Watch('showQrCodeResult') qrCodeParseResult: string = ''
  @State showResult: string = ""

  showQrCodeResult() {
    this.showResult = this.qrCodeParseResult
    if (this.showResult != "" && this.showResult != undefined) {
      router.replaceUrl({ url: "pages/Index", params: { scanData: this.showResult } })
      // 解析成功关闭相机
      CameraService.getInstance().destroy()
    }
  }

  aboutToAppear() {
    CameraService.getInstance().destroy()
  }

  onPageShow() {
    AppStorage.setOrCreate("qrCodeParseResult", "")
    CameraService.getInstance().init(getContext())
  }

  onPageHide() {
    CameraService.getInstance().release()
  }

  build() {
    Stack() {
      CameraView()
      if (!!this.showResult) {
        Text(this.showResult)
          .width('100%')
          .position({ x: 0, y: 600 })
          .borderColor(Color.White)
          .borderWidth(1)
          .fontColor(Color.White)
          .textAlign(TextAlign.Center)
      }
    }.width("100%").height("100%")
  }
}